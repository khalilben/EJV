package com.EJV.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.EJV.model.Jeux;

public interface IJeuxRepository extends JpaRepository<Jeux, Long> {

}
