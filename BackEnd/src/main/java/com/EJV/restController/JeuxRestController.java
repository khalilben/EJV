package com.EJV.restController;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.EJV.model.Jeux;
import com.EJV.service.IJeuxService;

@RestController
@CrossOrigin("*")
@RequestMapping("/rest/jeux")
public class JeuxRestController {

	@Autowired
	private IJeuxService jeuxService;

	@GetMapping("/getJeux")
	public List<Jeux> getAllJeux() {
		return jeuxService.getAllJeux();
	}

	@GetMapping("/getjeuxById")
	public Jeux getJeuxById(@RequestParam("idJeux") Long idJeux) {
		return jeuxService.getJeuxById(idJeux);
	}
}
