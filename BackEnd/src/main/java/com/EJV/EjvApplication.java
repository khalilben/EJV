package com.EJV;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EjvApplication {

	public static void main(String[] args) {
		SpringApplication.run(EjvApplication.class, args);
	}
}
