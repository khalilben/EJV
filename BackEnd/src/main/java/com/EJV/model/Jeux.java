package com.EJV.model;


import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by simoj on 20/05/2017.
 */

@Entity
public class Jeux implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idJeux;

	@Column(name = "nom")
	private String nom;

	@Column(name = "description")
	private String description;

	@Column(name = "prix")
	private String prix;

	@Column(name = "url_video")
	private String urlVideo;

	@Column(name = "url_image")
	private String urlImage;

	@ManyToOne
	@JoinColumn(name = "id_categorie", referencedColumnName = "id_categorie")
	private Categorie categorie;

	public Long getIdJeux() {
		return idJeux;
	}

	public void setIdJeux(Long idJeux) {
		this.idJeux = idJeux;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPrix() {
		return prix;
	}

	public void setPrix(String prix) {
		this.prix = prix;
	}

	public String getUrlVideo() {
		return urlVideo;
	}

	public void setUrlVideo(String urlVideo) {
		this.urlVideo = urlVideo;
	}

	public String getUrlImage() {
		return urlImage;
	}

	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public Jeux(String nom, String description, String prix, String urlVideo, String urlImage, Categorie categorie) {
		super();
		this.nom = nom;
		this.description = description;
		this.prix = prix;
		this.urlVideo = urlVideo;
		this.urlImage = urlImage;
		this.categorie = categorie;
	}

	public Jeux() {
		super();
		// TODO Auto-generated constructor stub
	}

}
