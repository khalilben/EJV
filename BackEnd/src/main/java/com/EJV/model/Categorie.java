package com.EJV.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by simoj on 20/05/2017.
 */

@Entity
public class Categorie implements Serializable {

	@Id
	@Column(name = "id_categorie")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idCategorie;

	@Column(name = "nom")
	private String nom;

	public Long getIdCategorie() {
		return idCategorie;
	}

	public void setIdCategorie(Long idCategorie) {
		this.idCategorie = idCategorie;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Categorie(String nom) {
		this.nom = nom;
	}

	public Categorie() {
	}
}
