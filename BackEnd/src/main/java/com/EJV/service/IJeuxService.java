package com.EJV.service;

import java.util.List;

import com.EJV.model.Jeux;

public interface IJeuxService {

	public List<Jeux> getAllJeux();

	public Jeux getJeuxById(Long idJeux);
}
