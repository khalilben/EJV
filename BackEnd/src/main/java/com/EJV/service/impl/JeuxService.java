package com.EJV.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.EJV.dao.IJeuxRepository;
import com.EJV.model.Jeux;
import com.EJV.service.IJeuxService;

@Service
@Transactional
public class JeuxService implements IJeuxService {

	@Autowired
	private IJeuxRepository jeuxRepository;

	@Override
	public List<Jeux> getAllJeux() {
		return jeuxRepository.findAll();
	}

	@Override
	public Jeux getJeuxById(Long idJeux) {
		return jeuxRepository.findOne(idJeux);
	}
}
